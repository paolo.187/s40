const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const	auth = require ("../auth");




//create product (Admin only)
router.post("/",auth.verify, (req,res)=> {

		const userData = auth.decode(req.headers.authorization)

		if (userData.isAdmin == true){
			productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
		}else{
			console.log("not authorized")
			res.send("not authorized")
		}
	 
})

//retrieve ALL products

router.get("/allProducts", (req,res)=> {
	productController.allProducts().then(resultFromController=>
		res.send(resultFromController))
})		


//retrieve all ACTIVE products
router.get("/allAvailable", (req,res)=>{
	productController.getAllAvailable().then(resultFromController =>
		res.send(resultFromController))
})


//retrieve SPECIFIC product by Id
router.get("/:productId", (req,res)=> {
	

	productController.getProduct(req.params).then(resultFromController =>
		res.send(resultFromController))
})

//Archive product by Id (Admin)

router.put("/:productId/archive", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization)


		if (userData.isAdmin == true){
			productController.archiveProduct(req.params, req.body).then(resultFromController =>
			res.send(resultFromController))
		}else{
			console.log("not authorized")
			res.send("not authorized")
		}

	
})

//Update Product Info(Admin)


router.put("/:productId", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == true){
		productController.updateProduct(req.params, req.body).then(resultFromController =>
		res.send(resultFromController))

	}else{
		res.send("not authorized")
	}
	
})









module.exports = router;
